return function(self)
    love.graphics.draw(self.bg, 0, 0, 0, self.bgScaleX, self.bgScaleY)
    local font = love.graphics.newFont("assets/fonts/bold.ttf",50)
    love.graphics.setFont(font)
    love.graphics.printf("WINNER",0,15,self.width,"center")
    local font = love.graphics.newFont("assets/fonts/regular.ttf",50)
    love.graphics.setFont(font)
    love.graphics.printf("Press space or start to play again",0,self.height - 80,self.width,"center")
    love.graphics.setColor(255,255,0)
    love.graphics.setColor(unpack(self.leaderboard[1].color))
    love.graphics.rectangle("fill",self.width * 0.15, 90,self.width * 0.7, 180)
    love.graphics.setColor(255,255,255)
    local scale = 160/self.images[self.leaderboard[1].animal]:getHeight()
    love.graphics.draw(self.images[self.leaderboard[1].animal],self.width * 0.15 + 50, 100,scale,scale)
    scale = 80/self.sombrero:getHeight()
    for i = 0, self.leaderboard[1].score-1 do
        love.graphics.draw(self.sombrero,self.width * 0.15 + 250 + (i*190), 140,scale,scale)
    end
    for i = 2, #self.leaderboard do
        love.graphics.setColor(unpack(self.leaderboard[i].color))
        love.graphics.rectangle("fill",self.width * 0.25, 290 + 120 * (i - 2),self.width/2,100)
        love.graphics.setColor(255,255,255)
        local scale = 80/self.images[self.leaderboard[i].animal]:getHeight()
        love.graphics.draw(self.images[self.leaderboard[i].animal],self.width * 0.25 + 30, 300 + 120 * (i - 2),scale,scale)
        scale = 50/self.sombrero:getHeight()
        for j = 0, self.leaderboard[i].score-1 do
            love.graphics.draw(self.sombrero,self.width * 0.25 + 150 + j*150, 315 + 120 * (i - 2),scale,scale)
        end
    end
    love.graphics.setColor(255,255,0)
    love.graphics.setColor(255,255,255)
end