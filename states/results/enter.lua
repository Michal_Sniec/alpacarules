return function(self)
    self.width = love.graphics.getWidth()
    self.height = love.graphics.getHeight()
    self.images = { love.graphics.newImage('assets/animalsStatic/ocelot.png'),
                    love.graphics.newImage('assets/animalsStatic/dog.png'),
                    love.graphics.newImage('assets/animalsStatic/iguana.png'),
                    love.graphics.newImage('assets/animalsStatic/armadillo.png')          
    }
    self.sombrero = love.graphics.newImage('assets/sombrero.png')
    self.getColorForIndex = function(i)
        if i == 1 then
            return {100,100,255,128}
        elseif i == 2 then
            return {255,165,0,128}
        elseif i == 3 then
            return {255,255,0,128}
        elseif i == 4 then
            return {184,3,255,128}
        end
    end
    self.leaderboard = {}
    for k,v in pairs(chosenAnimals) do
        local c = self.getColorForIndex(k)
        self.leaderboard[k] = {index = k, animal = v, score = playerScores[k],color = c}
    end
    self.bg = love.graphics.newImage('assets/menu/bg.png')
    self.bgScaleX = self.width/self.bg:getWidth()
    self.bgScaleY = self.height/self.bg:getHeight()
    table.sort(self.leaderboard,function(l,r)
        return l.score > r.score
    end)
    -- for k,v in pairs(self.leaderboard) do
    --     print(k.." "..v.score)
    -- end
end