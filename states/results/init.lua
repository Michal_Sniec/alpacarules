return {
    draw = require "states.results.draw",
    enter = require "states.results.enter",
    update = require "states.results.update",
    keypressed = require "states.results.keypressed",
    gamepadpressed = require "states.results.gamepadpressed"
}