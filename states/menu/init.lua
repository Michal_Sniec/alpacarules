return {
    draw = require "states.menu.draw",
    enter = require "states.menu.enter",
    update = require "states.menu.update",
    keypressed = require "states.menu.keypressed",
    gamepadpressed = require "states.menu.gamepadpressed"
}