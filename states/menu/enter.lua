return function(self)
    self.width = love.graphics.getWidth()
    self.height = love.graphics.getHeight()
    local joysticks = love.joystick.getJoysticks()
    self.joystick = joysticks[1]
    self.titleFont = love.graphics.newFont('assets/fonts/bold.ttf',40)
    self.subTitleFont = love.graphics.newFont('assets/fonts/regular.ttf',38)
    self.kevinFont = love.graphics.newFont('assets/fonts/regular.ttf',16)
    self.bg = love.graphics.newImage('assets/menu/bg.png')
    self.bgScaleX = self.width/self.bg:getWidth()
    self.bgScaleY = self.height/self.bg:getHeight()
    self.titleImg = love.graphics.newImage('assets/menu/napis.png')
    self.titleScale =  self.width * 0.5 / self.titleImg:getWidth()
    self.alpaca = love.graphics.newImage('assets/menu/alpaca.png')
    self.corals = { love.graphics.newImage('assets/menu/corals1.png'),
                    love.graphics.newImage('assets/menu/corals2.png'),
                    love.graphics.newImage('assets/menu/corals3.png')
    }
    self.coralsArr = {1,2,3,2}
    self.active = 1
    self.coralsWait = false
    timer.every(1/8,function()
        if not self.coralsWait then
            self.active = self.active + 1
            if self.active > #self.coralsArr then
                self.coralsWait = true
                timer.after(math.random(1,3),function()
                    self.coralsWait = false
                end)
                self.active = 1
            end
        end
    end)
end
