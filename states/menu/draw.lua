return function(self)
    local gw, gh = love.graphics.getWidth(), love.graphics.getHeight()

    love.graphics.draw(self.bg, 0, 0, 0, self.bgScaleX, self.bgScaleY)
    love.graphics.draw(self.alpaca, -100, 0, 0, self.bgScaleY, self.bgScaleY)
    love.graphics.draw(self.titleImg,self.width * 0.45 , 100,0,self.titleScale, self.titleScale)
    love.graphics.draw(self.corals[self.coralsArr[self.active]], -100, 0, 0, self.bgScaleY, self.bgScaleY)
    love.graphics.setFont(self.titleFont)
    love.graphics.setColor(243,225,101)
    love.graphics.printf("shock your enemies!",0,  250, self.width * 0.95,"right")
    love.graphics.setColor(255, 255, 255)
    love.graphics.setFont(self.subTitleFont)
    love.graphics.printf("Press spacebar or start to play", gw * 0.5 + 40, gh * (0.5 + 0.125 * 0.5) + 16, gw * 0.5 - 128 - 32, "center")

    love.graphics.setColor(255, 213, 137, 128)
    -- love.graphics.rectangle("fill", gw * 0.5 + 32, gh * 0.5, gw * 0.5 - 128, gh * 0.25)
    love.graphics.setColor(255, 255, 255)
    love.graphics.printf(
        [[
        Controls:
        Left or Right Sticks & Triggers on joypads
        ]],
        gw * 0.5 + 8,
        self.height - 200,
        gw * 0.5 - 128 - 32,
        "center"
    )

    love.graphics.setColor(255, 255, 255)
    love.graphics.setFont(self.kevinFont)
    love.graphics.print("Music by Kevin Macleod", 8, gh - 16 - 12)

    love.graphics.setColor(255, 255, 255)
end
