return {
    draw = require "states.playerSelect.draw",
    enter = require "states.playerSelect.enter",
    update = require "states.playerSelect.update",
    keypressed = require "states.playerSelect.keypressed",
    gamepadpressed = require "states.playerSelect.gamepadpressed"
}