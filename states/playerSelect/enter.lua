local playerSelectController = require("physics.playerSelectController")

return function(self)
    self.width = love.graphics.getWidth()
    self.height = love.graphics.getHeight()
    self.bg = love.graphics.newImage('assets/playerSelect/bg.png')
    self.lines = love.graphics.newImage('assets/playerSelect/lines.png')
    self.bgScaleX = self.width/self.bg:getWidth()
    self.bgScaleY = self.height/self.bg:getHeight()
    local joysticks = love.joystick.getJoysticks()
    self.joystick = joysticks[1]
    self.images = { love.graphics.newImage('assets/animalsStatic/ocelot.png'),
                    love.graphics.newImage('assets/animalsStatic/dog.png'),
                    love.graphics.newImage('assets/animalsStatic/iguana.png'),
                    love.graphics.newImage('assets/animalsStatic/armadillo.png')          
    }
    local p1 = playerSelectController:new(1,self.images,self.bgScaleX,self.bgScaleY)
    local p2 = playerSelectController:new(2,self.images,self.bgScaleX,self.bgScaleY)
    self.players = {p1,p2}
    signal.register("addPlayer",function()
        if(#self.players <= 3) then
            local p = playerSelectController:new(#self.players+1,self.images,self.bgScaleX,self.bgScaleY)
            lume.push(self.players,p)
        end
    end)
    signal.register("checkLock",function()
        chosenAnimals = {}
        for k,v in pairs(self.players) do
            if not v.locked then
                return
            else
                chosenAnimals[k]  = v.chosenIndex
            end
        end
        gamestate.switch(states.game)
    end)

    
end