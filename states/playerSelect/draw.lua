return function(self)
    love.graphics.setColor(255,255,255)
    love.graphics.draw(self.bg,0,0,0,self.bgScaleX,self.bgScaleY)
    love.graphics.draw(self.lines,0,0,0,self.bgScaleX,self.bgScaleY)
    if(#self.players < 4) then
        local ind = #self.players - 2
        local posX = ind * self.width/2
        love.graphics.setColor(0,0,0,128)
        love.graphics.rectangle("fill",posX,self.height/2,self.width/2,self.height/2)
        love.graphics.setColor(255,255,255,255)
        
        love.graphics.printf("Press enter or start to add player", posX, self.height * 3 / 4, self.width/2,"center")
    end
    for k,v in pairs(self.players) do
        v:draw()
    end
end