return function(self, dt)
    self.physics:update(dt)
    self.baloonSpawner:update()
    self.lightningSpawner:update(dt)
    for k,v in pairs(self.physics.world:getContactList()) do
        -- if not v then return end
        local fixtureA
        local fixtureB
        fixtureA, fixtureB = v:getFixtures()
        typeA = fixtureA:getUserData().type
        typeB = fixtureB:getUserData().type
        -- print(typeA .." colliding with ".. typeB)
        if (typeA == "player" and typeB == "lightning") or (typeB == "player" and typeA == "lightning") then
            -- print("player lightning")
            if typeA == "player" then
                if fixtureB:getUserData().active then
                    fixtureA:getUserData().entity:getHit(fixtureB:getUserData().entity,10)
                    fixtureB:getUserData().entity.toDestroy = true
                    print(fixtureA:getUserData().entity.hitPoints)
                end
            else
                if fixtureA:getUserData().active then
                    fixtureB:getUserData().entity:getHit(fixtureA:getUserData().entity,10)
                    fixtureA:getUserData().entity.toDestroy = true
                    print(fixtureB:getUserData().entity.hitPoints)
                end
            end
        end
    end

    self.lightningMod = math.max(self.lightningMod - dt, 0)
end
