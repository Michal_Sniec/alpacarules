return {
    draw = require "states.game.draw",
    enter = require "states.game.enter",
    update = require "states.game.update",
    keypressed = require "states.game.keypressed",
    signals = require "states.game.signals",
    gamepadpressed = require "states.game.gamepadpressed"
}