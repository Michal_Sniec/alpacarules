local physics = require "physics.physics"
local player = require "physics.player"
local baloonSpawner = require "physics.baloonSpawner"
local obstacleSpawner = require "physics.obstacleSpawner"
local collisionControler = require "physics.collisionController"
local alpaca = require "physics.alpaca"
local wall = require "physics.wall"
local lightningSpawner = require "physics.lightningSpawner"
local lantern = require "physics.lantern"

return function(self)
    bgScale = 0.75 * love.graphics.getHeight() / assets.level.bg:getHeight()

    local joysticks = love.joystick.getJoysticks()
    self.joystick = joysticks[1]
    self.wonGame = false
    math.randomseed(os.time())
    self.physics = physics:new()
    self.physics.world:setCallbacks(
        collisionControler["contact"],
        collisionControler["finishContact"],
        collisionControler["preSolve"],
        collisionControler["postSolve"]
    )
    self.players = {}
    for k,v in pairs(chosenAnimals) do
        self.players[k] = player:new(self.physics, k, v)
    end
    alivePlayers = {}
    for i = 1, #chosenAnimals do
        table.insert(alivePlayers, i)
    end

    player.scatter(self.players)
    signal.register('gameEnd',function()
        playerScores[alivePlayers[1]] = playerScores[alivePlayers[1]] + 1
        print("recive" .. alivePlayers[1])
        self.wonGame = true
        timer.after(3,function()
            if playerScores[alivePlayers[1]] == 3 then
                gamestate.switch(states.results)
            else
                gamestate.switch(states.game)
            end
        end)
        signal.clear("gameEnd")
    end)
    self.baloonSpawner = baloonSpawner:new(self.physics, #self.players)

    self.alpaca = alpaca:new(self.physics)


    self.walls = lume.each({
        "top", "bottom", "left", "right"
    }, function(side)
        return wall:new(self.physics, side)
    end)

    self.obstacleSpawner = obstacleSpawner:new(self.physics)
    self.lightningSpawner = lightningSpawner:new(self.physics)

    self.lightningMod = 0

    self.lantern1 = lantern:new(self.physics, assets.level.lanternTop, 18, true)
    self.lantern1.body:setPosition(
        love.graphics.getWidth() * 0.5,
        self.lantern1.bgScale * 621
    )

    self.lantern2 = lantern:new(self.physics, assets.level.lanternSmall, 36)
    self.lantern2.body:setPosition(
        love.graphics.getWidth() * 0.5 - self.lantern2.bgScale * (914 - 134),
        self.lantern2.bgScale * (1114 - 60)
    )

    self.lantern2.scaleX = -1

    self.lantern3 = lantern:new(self.physics, assets.level.lanternSmall, 36)
    self.lantern3.body:setPosition(
        love.graphics.getWidth() * 0.5 + self.lantern3.bgScale * (1030 - 0),
        self.lantern3.bgScale * (1126 - 28)
    )

    self.lantern4 = lantern:new(self.physics, assets.level.lanternBig, 50)
    self.lantern4.body:setPosition(
        love.graphics.getWidth() * 0.5 - self.lantern4.bgScale * (1220 - 40),
        self.lantern4.bgScale * (2695 - 300)
    )
    self.lantern4.scaleX = -1

    self.lantern5 = lantern:new(self.physics, assets.level.lanternBig, 50)
    self.lantern5.body:setPosition(
        love.graphics.getWidth() * 0.5 + self.lantern5.bgScale * (1308 - 20),
        self.lantern5.bgScale * (2495 - 200)
    )
end
