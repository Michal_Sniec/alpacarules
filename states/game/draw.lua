return function(self)
    love.graphics.setColor(255, 255, 255)
    local gw, gh = love.graphics.getWidth(), love.graphics.getHeight()

    local abgScale = gh / assets.level.bg:getHeight()
    local brickScale = 0.82
    love.graphics.draw(assets.level.bg, gw * 0.5, gh * 0.5, 0, abgScale, abgScale, assets.level.bg:getWidth() * 0.5, assets.level.bg:getHeight() * 0.5)
    love.graphics.draw(assets.level.bricks, gw * 0.5, 605 * bgScale, 0, bgScale * brickScale, bgScale * brickScale, assets.level.bricks:getWidth() * 0.5, 0)

    self.physics:draw()

    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(assets.level.lanternLights, gw * 0.5 + 46 * bgScale, 203 * bgScale, 0, bgScale, bgScale, assets.level.lanternLights:getWidth() * 0.5, 0)

    love.graphics.setColor(255, 255, 255, self.lightningMod * 80)
    love.graphics.rectangle("fill", 0, 0, gw, gh)

    if self.wonGame then
        love.graphics.setColor(0,0,0,128)
        love.graphics.rectangle("fill",0,0,love.graphics.getWidth(),love.graphics.getHeight())
        love.graphics.setColor(unpack(self.players[alivePlayers[1]].color))
        local font = love.graphics.newFont("assets/fonts/bold.ttf",50)
        love.graphics.setFont(font)
        love.graphics.printf("Player "..alivePlayers[1].." won this round",0,love.graphics.getHeight()/2 - 25,love.graphics.getWidth(),"center")
    end
end
