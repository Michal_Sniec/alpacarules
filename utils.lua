local utils = {}
utils.getItemIndex = function(arr,search)
    local k
    local v
    for k,v in pairs(arr) do
        -- print(k.." "..v.." " .. search)
        if(v == search) then
            return k
        end
    end
end
utils.arrayPrint = function(arr)
    for k,v in pairs(arr) do
         print(k.." "..v)
    end
end
return utils