local entity = require("physics.entity")

local alpaca = class("alpaca")

function alpaca:initialize(physics)
    entity.initialize(self)

    self.radius = 32

    self.body = love.physics.newBody(physics.world, love.graphics.getWidth() * 0.5, love.graphics.getHeight() * 0.5 - 350 * bgScale, "static")
    self.shape = love.physics.newCircleShape(self.radius)
    self.fixture = love.physics.newFixture(self.body, self.shape)

    self.fixture:setUserData {
        type = "alpaca",
        entity = self
    }

    self.sensor = {}
    self.sensor.radius = 80
    self.sensor.body = love.physics.newBody(physics.world, love.graphics.getWidth() * 0.5, love.graphics.getHeight() * 0.5, "static")
    self.sensor.shape = love.physics.newCircleShape(self.sensor.radius)
    self.sensor.fixture = love.physics.newFixture(self.sensor.body, self.sensor.shape)
    self.sensor.fixture:setSensor(true)
    self.sensor.fixture:setUserData {
        type = "alpacaSensor",
        entity = self
    }

    self.animationImage = love.graphics.newImage("assets/alpaca.png")
    self.animationFrameWidth = self.animationImage:getWidth() / 3
    self.animationFrameHeight = self.animationImage:getHeight() / 2
    self.animationGrid = anim8.newGrid(self.animationFrameWidth, self.animationFrameHeight, self.animationImage:getWidth(), self.animationImage:getHeight())
    self.animation = anim8.newAnimation(self.animationGrid('1-3', 1, '1-2', 2), 1)
    self.animation:flipH()

    entity.register(self, physics)

    self.loadingPlayers = 0
    self.currentFrame = 1
    self.frameChangeTime = 0
    self:resetFrameChangeTime()
    signal.register("startedLoading", function()
        self.loadingPlayers = self.loadingPlayers + 1
    end)
    signal.register("endedLoading", function()
        self.loadingPlayers = math.max(self.loadingPlayers - 1, 0)
    end)
end

function alpaca:resetFrameChangeTime()
    self.frameChangeTime = lume.random(2, 4)
end

function alpaca:draw()
    entity.draw(self)

    local scale = 0.25

    love.graphics.setColor(255, 255, 255)
    self.animation:draw(self.animationImage, self.body:getX(), self.body:getY() - 100 * bgScale, 0, scale, scale, self.animationFrameWidth * 0.5, self.animationFrameHeight * 0.5)

    -- love.graphics.setColor(255, 0, 0, 128)
    -- love.graphics.circle("fill", self.body:getX(), self.body:getY(), self.radius)
    -- love.graphics.circle("fill", self.body:getX(), self.body:getY(), self.sensor.radius)
end

function alpaca:update(dt)
    entity.update(self, dt)

    if self.frameChangeTime > 0 then
        self.frameChangeTime = self.frameChangeTime - dt
        if self.frameChangeTime <= 0 then
            local behavior = lume.randomchoice({
                {
                    frame = 3,
                    time = 1.5
                },
                {
                    frame = 4,
                    time = 0.5
                },
                {
                    frame = 5,
                    time = 0.2
                }
            })
            timer.script(function(wait)
                self.currentFrame = behavior.frame
                wait(behavior.time)
                self.currentFrame = 1
                self:resetFrameChangeTime()
            end)
        end
    end

    if self.loadingPlayers > 0 then
        self.animation:gotoFrame(2)
    else
        self.animation:gotoFrame(self.currentFrame)
    end
end

return alpaca
