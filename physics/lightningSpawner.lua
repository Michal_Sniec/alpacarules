local lightning = require "physics.lightning"

local lightningSpawner = class("lightningSpawner")

function lightningSpawner:initialize(physics)
    self.spawnIndex = 1
    self.spawnTime = 0
    self.physics = physics
    self:resetTime()
end

function lightningSpawner:resetTime()
    self.spawnTime = (0.3 * self.spawnIndex + 3.8) / (0.35 * self.spawnIndex + 0.45)
end

function lightningSpawner:update(dt)
    self.spawnTime = self.spawnTime - dt
    if self.spawnTime <= 0 then
        self.spawnIndex = self.spawnIndex + 0.1
        self:resetTime()
        lightning:new(self.physics)
    end
end

return lightningSpawner
