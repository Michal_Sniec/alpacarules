local entity = require "physics.entity"

local baloon = class("baloon")


function baloon:initialize(baloonSpawner)
    local physics = baloonSpawner.physics
    entity.initialize(self, physics)
    local maxWidth = love.graphics.getWidth()
    local maxHeight = love.graphics.getHeight()
    local x
    local y
    repeat
        x = math.random(20, maxWidth - 20)
        y = math.random(20, maxHeight - 20)
    until not((x > maxWidth / 2 - 80 and x < maxWidth / 2 + 80) and (y > maxHeight / 2 - 80 and y < maxHeight / 2 + 80))
    self.radius = 12
    self.baloonSpawner = baloonSpawner
    self.body = love.physics.newBody(physics.world, x, y, "static")
    self.shape = love.physics.newCircleShape(self.radius)
    self.fixture = love.physics.newFixture(self.body, self.shape)
    self.fixture:setSensor(true)

    self.fixture:setUserData {
        type = "baloonStatic",
        entity = self
    }

    self.color = {
        math.random(100, 255),
        math.random(100, 255),
        math.random(100, 255),
    }

    self.image = assets.balloon

    entity.register(self, physics)
end

function baloon:draw()
    entity.draw(self)

    local scale = 0.03

    love.graphics.setColor(unpack(self.color))
    love.graphics.draw(self.image, self.body:getX(), self.body:getY(), self.body:getAngle(), scale, scale, self.image:getWidth() * 0.5, self.image:getHeight() * 0.5)
end

function baloon:update()
    entity.update(self)
end
function baloon:collect(player)
    player:setHoldingBalloon(true, self.color)
    self.baloonSpawner.baloonCounter = self.baloonSpawner.baloonCounter - 1
    self.baloonSpawner.next = true
    self.toDestroy = true
end

return baloon
