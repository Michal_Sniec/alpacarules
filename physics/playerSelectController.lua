local controls = require "settings.controls"
local playerSelectController = class("playerSelectController")

function playerSelectController:initialize(index,images,scaleX,scaleY)

    self.index = index
    self.chosenIndex = 1
    self.width = love.graphics.getWidth()
    self.height = love.graphics.getHeight()
    self.blocked = false
    self.locked = false
    self:resetParameters()
    self:getColorForIndex()
    
    local logicalIndex = self.index - 1
    self.posX = 10 + (logicalIndex%2) * self.width/2
    self.posY = 10 + math.floor(logicalIndex/2) * self.height/2

    self.images = images
    self.scales = { (self.height/2-40)/self.images[1]:getHeight(),
        (self.height/2-40)/self.images[2]:getHeight(),
        (self.height/2-40)/self.images[3]:getHeight(),
        (self.height/2-40)/self.images[4]:getHeight()
    }
    self.scaleX = scaleX
    self.scaleY = scaleY
    self.statsPanel = love.graphics.newImage('assets/playerSelect/statsPanel.png')
    self.speed = love.graphics.newImage('assets/playerSelect/speed.png')
    self.hp = love.graphics.newImage('assets/playerSelect/hp.png')
    self.dmg = love.graphics.newImage('assets/playerSelect/dmg.png')
    self.charge = love.graphics.newImage('assets/playerSelect/charge.png')
    self.nameFont = love.graphics.newFont('assets/fonts/bold.ttf',28)
    self.regularFont = love.graphics.newFont('assets/fonts/regular.ttf',24)
    signal.register("nextAnimal", function(index)
        if index == self.index then
            self.blocked = true
            if(self.chosenIndex == #self.images) then
                self.chosenIndex = 1
            else
                self.chosenIndex = self.chosenIndex + 1
            end
            self:resetParameters()
        end
    end)


    signal.register("previousAnimal", function(index)
        if index == self.index then
            self.blocked = true
            if(self.chosenIndex == 1) then
                self.chosenIndex = #self.images
            else
                self.chosenIndex = self.chosenIndex - 1
            end
            self:resetParameters()            
        end
    end)

    self.controls = baton.new {
        controls = controls[self.index].controls,
        joystick = love.joystick.getJoysticks()[controls[self.index].joystickIndex]
    }
    
end

function playerSelectController:draw()
    love.graphics.setColor(255,255,255)
    local ind = self.chosenIndex
    love.graphics.draw(self.images[ind],self.posX,self.posY,0,self.scales[ind],self.scales[ind])
    local panelWidth = self.statsPanel:getWidth() * self.scaleX
    local statsPanelX = self.width/2 + self.width/2 * ((self.index-1)%2) - panelWidth - 30
    love.graphics.draw(self.statsPanel,statsPanelX, self.posY + 20, 0, self.scaleX, self.scaleY)
    love.graphics.draw(self.speed,statsPanelX + panelWidth * 0.1, self.posY + 80, 0, 30/self.speed:getWidth(), 30/self.speed:getWidth())
    love.graphics.draw(self.hp,statsPanelX + panelWidth * 0.6, self.posY + 90, 0, 30/self.hp:getWidth(), 30/self.hp:getWidth())
    love.graphics.draw(self.dmg,statsPanelX + panelWidth * 0.1, self.posY + 180, 0, 30/self.dmg:getWidth(), 30/self.dmg:getWidth())
    love.graphics.draw(self.charge,statsPanelX + panelWidth * 0.56, self.posY + 173, 0, 50/self.charge:getWidth(), 50/self.charge:getWidth())
    local offset = 50
    love.graphics.setFont(self.regularFont)
    love.graphics.print(self.speedVal,statsPanelX + panelWidth * 0.1 + offset, self.posY + 80)
    love.graphics.print(self.chargeSpeedVal,statsPanelX + panelWidth * 0.6 + offset, self.posY + 80)
    love.graphics.print(self.dmgAmplifierVal,statsPanelX + panelWidth * 0.1 + offset, self.posY + 180)
    love.graphics.print(self.hitPointsVal,statsPanelX + panelWidth * 0.56 + 10 + offset, self.posY + 180)
    love.graphics.printf("press action to play",statsPanelX , self.height / 2 + self.height / 2 * math.floor((self.index-1)/2)-60,panelWidth,"center")
    -- love.graphics.printf("Charge speed:".. self.chargeSpeed,self.posX + self.scales[ind] * self.images[ind]:getWidth() + 20, self.posY + 40,self.width/4,"left")
    -- love.graphics.printf("Damage amplifier:".. self.dmgAmplifier,self.posX + self.scales[ind] * self.images[ind]:getWidth() + 20, self.posY + 120,self.width/4,"left")
    -- love.graphics.printf("Hit points:".. self.hitPoints,self.posX + self.scales[ind] * self.images[ind]:getWidth() + 20, self.posY + 200,self.width/4,"left")
    love.graphics.setColor(unpack(self.color))
    love.graphics.setFont(self.nameFont)
    love.graphics.printf(self.name,statsPanelX, self.posY + 25, panelWidth,"center")
    if self.locked then
        local logicalIndex = self.index - 1

        love.graphics.setColor(unpack(lume.concat(self.color,{96})))
        love.graphics.rectangle("fill",self.width/2 * (logicalIndex%2), self.height/2 * math.floor(logicalIndex/2),self.width/2,self.height/2)
    end
    -- love.graphics.printf()
end

function playerSelectController:update(dt)
    
    self.controls:update()

    if not self.locked then
        local left =  self.controls:get "left"
        if left > 0.8 and not self.blocked then
            signal.emit("nextAnimal",self.index)
        end
        local right =  self.controls:get "right"
        if right > 0.8 and not self.blocked then
            signal.emit("previousAnimal",self.index)
        end
        if right < 0.2 and left <0.2 then
            self.blocked = false
        end

        local action = self.controls:get "action"
        if action > 0.5 then
            self.locked = true
            signal.emit("checkLock")
        end
    end
end

function playerSelectController:getColorForIndex()
    if self.index == 1 then
        self.color = {0,0,255}
    elseif self.index == 2 then
        self.color = {255,165,0}
    elseif self.index == 3 then
        self.color = {255,255,0}
    elseif self.index == 4 then
        self.color = {184,3,255}
    end
end

function playerSelectController:resetParameters()
    local parameters = playerTypes[self.chosenIndex]
    self.speedVal = parameters["speedShowable"]
    self.chargeSpeedVal = parameters["chargeSpeedShowable"]
    self.hitPointsVal = parameters["hpShowable"]
    self.dmgAmplifierVal = parameters["dmgAmplifierShowable"]
    self.name = parameters["name"]
end


return playerSelectController