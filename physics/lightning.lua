local entity = require("physics.entity")

local lightning = class("lightning")

function lightning:initialize(physics)
    entity.initialize(self)

    physicsGlobal = physics

    self.radius = 24
    self.radius2 = 16
    self.dmgAmplifier = 1
    self.body = love.physics.newBody(physics.world, lume.random(0, love.graphics.getWidth()), lume.random(0, love.graphics.getHeight()), "static")
    self.shape = love.physics.newCircleShape(self.radius)
    self.fixture = love.physics.newFixture(self.body, self.shape)

    self.fixture:setSensor(true)
    self.fixture:setUserData {
        type = "lightning",
        entity = self,
        active = false
    }

    self.modScale = 0

    self.animationImage = assets.thunder
    self.animationFrameWidth = self.animationImage:getWidth() / 5
    self.animationFrameHeight = self.animationImage:getHeight()
    self.animationGrid = anim8.newGrid(self.animationFrameWidth, self.animationFrameHeight, self.animationImage:getWidth(), self.animationImage:getHeight())
    self.animation = anim8.newAnimation(self.animationGrid('1-5', 1), 0.1)

    entity.register(self, physics)

    self.timer = timer.script(function(wait)
        wait(3)

        local ud = self.fixture:getUserData()
        ud.active = true
        self.fixture:setUserData(ud)

        gamestate.current().lightningMod = 1

        assets.thunderSound:play()

        wait(0.5)

        self.toDestroy = true
    end)
end

function lightning:draw()
    entity.draw(self)

    if not self.fixture:getUserData().active then
        love.graphics.setColor(128, 128, 255, 100)
    else
        love.graphics.setColor(200, 200, 255, 0)
    end
    love.graphics.ellipse("fill", self.body:getX(), self.body:getY(), self.modScale * self.radius, self.modScale * self.radius2)

    if self.fixture:getUserData().active then
        love.graphics.setColor(255, 255, 255)
        self.animation:draw(self.animationImage, self.body:getX(), self.body:getY(), 0, 0.7, 0.7, self.animationFrameWidth * 0.5, self.animationFrameHeight - self.radius2 * 2)
    end
end

function lightning:update(dt)
    entity.update(self, dt)

    self.modScale = math.min(self.modScale + dt / 3, 1)

    if self.modScale == 1 then
        self.animation:update(dt)
    end
end

return lightning
