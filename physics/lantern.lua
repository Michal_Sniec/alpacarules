local entity = require "physics.entity"

local lantern = class("lantern")

function lantern:initialize(physics, image, offsetY, hideShine)
    entity.initialize(self)

    self.image = image
    self.offsetY = offsetY
    self.scaleX = 1

    self.bgScale = bgScale

    self.hideShine = hideShine or false

    self.body = love.physics.newBody(physics.world, 0, 0, "static")
    self.shape = love.physics.newCircleShape(offsetY * self.bgScale)
    self.fixture = love.physics.newFixture(self.body, self.shape)

    self.fixture:setUserData {
        type = "lantern"
    }

    self.lanternScale = 1

    timer.after(lume.random(0, 2), function()
        timer.every(3, function()
            timer.script(function(wait)
                timer.tween(1.5, self, {lanternScale = 0.8}, "in-quad")
                wait(1.5)
                timer.tween(1.5, self, {lanternScale = 1}, "out-quad")
                wait(1.5)
            end)
        end)
    end)

    entity.register(self, physics)
end

function lantern:draw()
    entity.draw(self)

    if not self.hideShine then
        love.graphics.setColor(255, 255, 255, 100)
        love.graphics.draw(assets.lanternShine, self.body:getX(), self.body:getY() + self.bgScale * 90, 0, self.lanternScale, self.lanternScale, assets.lanternShine:getWidth() * 0.5, assets.lanternShine:getHeight() * 0.5)
    end
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(self.image, self.body:getX(), self.body:getY() + self.offsetY, 0, self.bgScale * self.scaleX, self.bgScale, self.image:getWidth() * 0.5, self.image:getHeight())
end

function lantern:update(dt)
    entity.update(self, dt)
end

return lantern
