local collisionController = {}
    collisionController.contact = function(bodyA,bodyB,collision)
        local typeA = bodyA:getUserData().type
        local typeB = bodyB:getUserData().type
        -- print(typeA .." colliding with ".. typeB)
        if (typeA == "player" and typeB == "baloonStatic") or (typeB == "player" and typeA == "baloonStatic") then
            if typeA == "player" then
                collisionController.betweenPlayerAndBaloonStatic(bodyA:getUserData().entity, bodyB:getUserData().entity)
            else
                collisionController.betweenPlayerAndBaloonStatic(bodyB:getUserData().entity, bodyA:getUserData().entity)
            end
        end
        if (typeA == "player" and typeB == "obstacle") or (typeB == "player" and typeA == "obstacle") then
            if typeA == "player" then
                collisionController.betweenPlayerAndObstacle(bodyA:getUserData().entity, bodyB:getUserData().entity)
            else
                collisionController.betweenPlayerAndObstacle(bodyB:getUserData().entity, bodyA:getUserData().entity)
            end
        end
        if (typeA == "playerBalloon" and typeB == "obstacle") or (typeB == "playerBalloon" and typeA == "obstacle") then
            if typeA == "playerBalloon" then
                collisionController.betweenPlayerBalloonAndObstacle(bodyA:getUserData().entity, bodyB:getUserData().entity)
            else
                collisionController.betweenPlayerBalloonAndObstacle(bodyB:getUserData().entity, bodyA:getUserData().entity)
            end
        end
        if (typeA == "playerBalloon" and typeB == "playerBalloon") then
            collisionController.betweenPlayerBalloonAndPlayerBallon(bodyA:getUserData().entity, bodyB:getUserData().entity)
        end
        if (typeA == "playerBalloon" and typeB == "alpacaSensor") or (typeB == "playerBalloon" and typeA == "alpacaSensor") then
            if typeA == "playerBalloon" then
                collisionController.betweenPlayerBalloonAndAlpacaSensor(bodyA:getUserData().entity)
            else
                collisionController.betweenPlayerBalloonAndAlpacaSensor(bodyB:getUserData().entity)
            end
        end

        if (typeA == "player" and typeB == "alpacaSensor") or (typeB == "player" and typeA == "alpacaSensor") then
            if typeA == "player" then
                collisionController.betweenPlayerAndAlpacaSensor(bodyA:getUserData().entity)
            else
                collisionController.betweenPlayerAndAlpacaSensor(bodyB:getUserData().entity)
            end
        end

        if (typeA == "playerBalloon" and typeB == "player") or (typeB == "playerBalloon" and typeA == "player") then
            if typeA == "playerBalloon" then
                collisionController.betweenPlayerBalloonAndPlayer(bodyA:getUserData().entity,bodyB:getUserData().entity)
            else
                collisionController.betweenPlayerBalloonAndPlayer(bodyB:getUserData().entity,bodyA:getUserData().entity)
            end
        end
    end
    collisionController.finishContact = function(bodyA,bodyB,collision)
        local typeA = bodyA:getUserData().type
        local typeB = bodyB:getUserData().type
        if (typeA == "playerBalloon" and typeB == "alpacaSensor") or (typeB == "playerBalloon" and typeA == "alpacaSensor") then
            if typeA == "playerBalloon" then
                collisionController.betweenPlayerBalloonAndAlpacaSensorStop(bodyA:getUserData().entity)
            else
                collisionController.betweenPlayerBalloonAndAlpacaSensorStop(bodyB:getUserData().entity)
            end
        end
        if (typeA == "player" and typeB == "alpacaSensor") or (typeB == "player" and typeA == "alpacaSensor") then
            if typeA == "player" then
                collisionController.betweenPlayerAndAlpacaSensorStop(bodyA:getUserData().entity)
            else
                collisionController.betweenPlayerAndAlpacaSensorStop(bodyB:getUserData().entity)
            end
        end
    end
    collisionController.preSolve = function(bodyA,bodyB,collision)
        -- print(bodyA:getUserData().type.." colliding with "..bodyB:getUserData().type)
    end
    collisionController.postSolve = function(bodyA,bodyB,collision)
        -- print(bodyA:getUserData().type.." colliding with "..bodyB:getUserData().type)
    end
    collisionController.betweenPlayerAndBaloonStatic = function(player,baloon)
        if not player:isHoldingBalloon() and not player.isDied then
            baloon:collect(player)
        end
    end
    collisionController.betweenPlayerAndObstacle = function(player,obstacle)
        player:getHit(obstacle,10)
    end
    collisionController.betweenPlayerBalloonAndObstacle = function(player)
        if player:isHoldingBalloon() then
            player:burstBalloon()
        end
    end
    collisionController.betweenPlayerBalloonAndPlayerBallon = function(player1, player2)
        if player1:isHoldingBalloon() and player2:isHoldingBalloon() then
            player1:burstBalloon()
            player2:burstBalloon()
        end
    end
    collisionController.betweenPlayerBalloonAndAlpacaSensor = function(player)

    end
    collisionController.betweenPlayerBalloonAndAlpacaSensorStop = function (player)

    end

    collisionController.betweenPlayerAndAlpacaSensor = function(player)
        if player:isHoldingBalloon() then
            player.alpacaCollision = true
        end
    end
    collisionController.betweenPlayerAndAlpacaSensorStop = function(player)
        player.alpacaCollision = false
    end

    collisionController.betweenPlayerBalloonAndPlayer = function (player1, player2)
        if(player1:isHoldingBalloon() and player1.balloon.load > 0 and not player2.isDied) then
            player2:getHit(player1,player1.balloon.load)
            player2.animationEffect:resume()
            player1:setHoldingBalloon(false)
        end
    end
    -- collisionController.betweenPlayerAndLightning = function(player, lighthning)
    --     if lighthning.getUserData().active then
    --         player.getHit(lighthning,10)
    --         print(player.hitPoints)
    --     end
    -- end
return collisionController
