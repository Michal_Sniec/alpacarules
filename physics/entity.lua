local entity = class('entity')

function entity:initialize(physics)
    
end

function entity:register(physics)
    if self.fixture and self.body and self.shape then
        lume.push(physics.entities, self)
    else
        error("This entity doesn't have a fixture")
    end
end

function entity:draw()

end

function entity:update(dt)
    if self.fixture and self.body and self.shape and self.toDestroy then
        self.fixture:destroy()
    end
end

return entity