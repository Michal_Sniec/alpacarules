local controls = require "settings.controls"
local entity = require "physics.entity"
local hitPointsBar = require "physics.hitPoitsBar"
local playerBalloon = require "physics.playerBalloon"

local player = class("player")

function player:initialize(physics, index, animalIndex)
    entity.initialize(self, physics)

    self.physics = physics

    self.index = index
    self.radius = 16
    local parameters = playerTypes[animalIndex]
    self.speed = parameters["speed"]
    self.chargeSpeed = parameters["chargeSpeed"]
    self.hitPoints = parameters["hp"]
    self.dmgAmplifier = parameters["dmgAmplifier"]
    self.isDied = false
    self.destAngle = 0
    self.destMove = vector(0, 0)
    self:getColorForIndex()

    self.alpacaCollision = false

    signal.register("move", function(index, moveX, moveY)
        if index == self.index and not self.isDied then
            local move = vector(moveX, moveY)

            move:trimInplace(1)

            self.destMove = move

            if move:len() ~= 0 then
                self.destAngle = move:toPolar().x
            end
        end
    end)

    self.body = love.physics.newBody(physics.world, 0, 0, "dynamic")
    self.shape = love.physics.newCircleShape(self.radius)
    self.fixture = love.physics.newFixture(self.body, self.shape)

    self.body:setLinearDamping(10)

    self.fixture:setUserData {
        type = "player",
        entity = self
    }

    self.balloon = nil

    self.controls = baton.new {
        controls = controls[self.index].controls,
        pairs = {
            move = {"left", "right", "up", "down"}
        },
        joystick = love.joystick.getJoysticks()[controls[self.index].joystickIndex]
    }

    self.hitPointsBar = hitPointsBar:new(self)

    self.animationImage = assets[parameters.image]
    self.animationFrameWidth = self.animationImage:getWidth() / parameters.tilesColumns
    self.animationFrameHeight = self.animationImage:getHeight() / parameters.tilesRows
    self.animationGrid = anim8.newGrid(self.animationFrameWidth, self.animationFrameHeight, self.animationImage:getWidth(), self.animationImage:getHeight())
    self.animationWalk = anim8.newAnimation(self.animationGrid(unpack(parameters.animationWalk)), 1 / 12)
    self.animationIdle = anim8.newAnimation(self.animationGrid(unpack(parameters.animationIdle)), 1)
    self.animationDied = anim8.newAnimation(self.animationGrid(unpack(parameters.animationDead)), 1)
    self.animationDirection = 1
    self.animationState = "Idle"

    local g = anim8.newGrid(128, 103, assets.effect:getWidth(), assets.effect:getHeight())
    self.animationEffect = anim8.newAnimation(g('1-6', 1), 0.05, "pauseAtStart")

    self.scale = 0.2

    self.sombreroShader = love.graphics.newShader([[
        extern vec4 playerColor;

        vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords) {
            vec4 texturecolor = Texel(texture, texture_coords);
            if (texturecolor.r > 0.9 && texturecolor.g > 0.0 && texturecolor.g < 0.95 && texturecolor.b > 0.9) {
                return texturecolor * playerColor;
            }
            return texturecolor * color;
        }
    ]], [[
        vec4 position(mat4 transform_projection, vec4 vertex_position) {
            return transform_projection * vertex_position;
        }
    ]]);
    local colorSend = lume.clone(self.color)
    lume.push(colorSend, 255)
    self.sombreroShader:sendColor("playerColor", colorSend)

    self.grayShader = love.graphics.newShader([[
        vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords) {
            vec4 texturecolor = Texel(texture, texture_coords);
            number meanIntensity = (texturecolor.r  + texturecolor.g + texturecolor.b) / 3.0f;
            vec4 meanColor = vec4(meanIntensity, meanIntensity, meanIntensity, texturecolor.a);
            return meanColor * color;
        }
    ]], [[
        vec4 position(mat4 transform_projection, vec4 vertex_position) {
            return transform_projection * vertex_position;
        }
    ]])

    entity.register(self, physics)
end

function player:draw()
    entity.draw(self)

    self.hitPointsBar:draw(self.hitPoints)

    love.graphics.setColor(255, 255, 255)
    love.graphics.setShader(self.sombreroShader)

    self["animation" .. self.animationState]:draw(self.animationImage, self.body:getX(), self.body:getY(), 0, self.animationDirection * self.scale, self.scale, self.animationFrameWidth * 0.5, self.animationFrameHeight * 0.5)
    love.graphics.setShader()

    love.graphics.setColor(255, 255, 255)
    self.animationEffect:draw(assets.effect, self.body:getX(), self.body:getY(), 0, 1, 1, 64, 103 / 2)

    -- love.graphics.setColor(255, 255, 255, 100)
    -- love.graphics.circle("fill", self.body:getX(), self.body:getY(), self.radius)
end

function player:update(dt)
    entity.update(self, dt)

    self.controls:update()

    self.animationEffect:update(dt)

    if self.destMove:len() == 0 then
        self.animationState = "Idle"
        self.animationIdle:update(dt)
    else
        self.animationState = "Walk"
        self.animationWalk:update(dt)
        local animDir = vector(self.destMove.x, 0):normalizeInplace().x
        if animDir ~= 0 then self.animationDirection = animDir end
    end

    if self.isDied then
        self.animationState = "Died"
    end

    local x, y = self.controls:get "move"
    if not self.isDied then
        signal.emit("move", self.index, x, y)
    end
    local action = self.controls:get "action"
    if action > 0.5 and self:isHoldingBalloon() and self.alpacaCollision then
        self.balloon.load = self.balloon.load + dt * self.chargeSpeed / 3
        if self.balloon.load >= self.balloon.loadMax then
            self:burstBalloon()
        end
    end

    if self:isHoldingBalloon() and self.alpacaCollision then
        if self.controls:pressed "action" then
            signal.emit "startedLoading"
            self.balloon.emitting = true
        end
        if self.controls:released "action" then
            signal.emit "endedLoading"
            self.balloon.emitting = false
        end
    end

    if self.balloonToCreate then
        self.balloon = playerBalloon:new(self.physics, self, self.balloonToCreate)
        local len = 48
        self.balloon.body:setPosition(self.body:getX(), self.body:getY() + len)
        self.balloon.fixture:setSensor(true)

        self.rope = love.physics.newRopeJoint(self.body, self.balloon.body, self.body:getX(), self.body:getY(), self.balloon.body:getX(), self.balloon.body:getY(), len + 16)

        self.balloonToCreate = nil
    end

    self.body:setAngle(-self.destAngle + math.pi / 2)
    self.body:setLinearVelocity(self.destMove.x * self.speed, self.destMove.y * self.speed)
    self.body:setAngularVelocity(0)
end

function player:getHit(source, damage)
    self.hitPoints =  self.hitPoints - damage * source.dmgAmplifier
    -- print(self.hitPoints)
    if self.hitPoints <= 0 and not self.isDied then
        self:die()
    end
end

function player:die()
    if #alivePlayers == 1 then return end
    self.isDied = true
    self.fixture:setSensor(true)
    local arrIndex = utils.getItemIndex(alivePlayers,self.index)
    table.remove(alivePlayers,arrIndex)
    self.destMove = vector(0, 0)
    if #alivePlayers == 1 then
        print("emit")
        signal.emit("gameEnd")
    end
end

function player:isHoldingBalloon()
    return not not self.balloon
end

function player:setHoldingBalloon(hold, balloonColor)
    if hold and not self:isHoldingBalloon() then
        self.balloonToCreate = balloonColor
    elseif not hold and self:isHoldingBalloon() then
        self.rope:destroy()
        self.rope = nil

        self.balloon.fixture:destroy()
        self.balloon = nil
    end
end

function player:burstBalloon()
    if self:isHoldingBalloon() then
        signal.emit "endedLoading"
        self.balloon.emitting = false
        self:setHoldingBalloon(false)
    end
end

function player:getColorForIndex()
    if self.index == 1 then
        self.color = {100,100,255}
    elseif self.index == 2 then
        self.color = {255,165,0}
    elseif self.index == 3 then
        self.color = {255,255,0}
    elseif self.index == 4 then
        self.color = {184,3,255}
    end
end
player.static.scatter = function(players)
    for key, player in pairs(players) do
        player.body:setPosition(
            (0.25 + 0.5 * ((key - 1) % 2)) * love.graphics.getWidth(),
            (0.25 + 0.5 * math.floor((key - 1) / 2)) * love.graphics.getHeight()
        )
    end
end

return player
