local entity = require("physics.entity")

local obstacle = class("obstacle")

function obstacle:initialize(physics)
    entity.initialize(self)

    local alpaca = (lume.filter(physics.entities, function(entity)
        return not entity.fixture:isDestroyed() and entity.fixture:getUserData().type == "alpaca"
    end))[1]

    self.radius = 16
    self.dmgAmplifier = 1
    local positionX, positionY
    while true do
        local breakFlag = true

        positionX, positionY =
            math.random(0, love.graphics.getWidth()),
            math.random(0, love.graphics.getHeight())

        physics.world:rayCast(
            positionX - self.radius * 2,
            positionY - self.radius * 2,
            positionX + self.radius * 2,
            positionY + self.radius * 2,
            function(f) breakFlag = false return 0 end
        )

        physics.world:rayCast(
            positionX + self.radius * 2,
            positionY - self.radius * 2,
            positionX - self.radius * 2,
            positionY + self.radius * 2,
            function(f) breakFlag = false return 0 end
        )

        if ((positionX - alpaca.body:getX()) ^ 2 + (positionY - alpaca.body:getY()) ^ 2 < (assets.level.bricks:getWidth() * 0.3) ^ 2) then
            breakFlag = false
        end

        if breakFlag then break end
    end
    -- print(positionY)
    self.body = love.physics.newBody(physics.world, positionX, positionY, "static")
    self.shape = love.physics.newCircleShape(self.radius)
    self.fixture = love.physics.newFixture(self.body, self.shape)

    self.fixture:setUserData {
        type = "obstacle",
        entity = self
    }

    self.image = assets.level["cactus" .. math.random(1, 2)]
    self.bgScale = bgScale

    entity.register(self, physics)
end

function obstacle:draw()
    entity.draw(self)

    local scale = 0.75

    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(self.image, self.body:getX(), self.body:getY() + 100 * self.bgScale, 0, self.bgScale * scale, self.bgScale * scale, self.image:getWidth() * 0.5, self.image:getHeight())

    -- love.graphics.circle("fill", self.body:getX(), self.body:getY(), self.radius)
end

function obstacle:update(dt)
    entity.update(self, dt)
end

return obstacle
