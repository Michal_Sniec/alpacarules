local entity = require("physics.entity")

local wall = class("wall")

function wall:initialize(physics, side)
    entity.initialize(self)

    local points = {}
    local w, h = love.graphics.getWidth(), love.graphics.getHeight()
    if side == "top" then points = {0, 0, w, 0} end
    if side == "bottom" then points = {0, h, w, h} end
    if side == "left" then points = {0, 0, 0, h} end
    if side == "right" then points = {w, 0, w, h} end

    self.body = love.physics.newBody(physics.world, 0, 0, "static")
    self.shape = love.physics.newEdgeShape(unpack(points))
    self.fixture = love.physics.newFixture(self.body, self.shape)

    self.fixture:setUserData {
        type = "wall",
        entity = self
    }
end

function wall:draw()
    love.graphics.setColor(255, 255, 255)
    love.graphics.setLineWidth(4)
    love.graphics.line(self.shape:getPoints())
    love.graphics.setLineWidth(1)
end

function wall:update(dt)

end

return wall