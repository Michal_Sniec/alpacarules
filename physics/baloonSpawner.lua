
local baloonSpawner = class("baloonSpawner")

local baloon = require"physics.baloon"

function baloonSpawner:initialize(physics,playerNumber)
    self.physics = physics
    self.baloonCounter = 0
    self.playerNumber = playerNumber
    self:spawn()
    signal.register("createBalloons",function()
        self:spawn()
    end)
end

function baloonSpawner:update()
    if(self.next) then
        self:spawn()
        self.next = false
    end
end

function baloonSpawner:spawn()
    local toRender = math.random(1,self.playerNumber - self.baloonCounter)
    -- print(toRender)
    for i=1, toRender do
        baloon:new(self)
    end
    self.baloonCounter = self.baloonCounter + toRender
end

return baloonSpawner