local entity = require "physics.entity"

local playerBalloon = class("playerBalloon")

function playerBalloon:initialize(physics, player, color)
    entity.initialize(self)

    self.radius = 12
    self.load = 0
    self.loadMax = 1
    self.body = love.physics.newBody(physics.world, 0, 0, "dynamic")
    self.shape = love.physics.newCircleShape(self.radius)
    self.fixture = love.physics.newFixture(self.body, self.shape)
    self.emitting = false

    self.body:setMass(10e-9)
    self.body:setLinearDamping(2)
    self.fixture:setUserData {
        type = "playerBalloon",
        entity = player
    }

    self.color = color

    self.image = assets.balloon

    self.emitter = love.graphics.newParticleSystem(assets.spark, 512)
    self.emitter:start()
    self.emitter:setSpeed(120)
    self.emitter:setSpin(-1, 1)
    self.emitter:setSpread(math.pi * 2)
    self.emitter:setColors(
        255, 255, 255, 255,
        255, 255, 255, 0
    )
    self.emitter:setEmitterLifetime(-1)
    self.emitter:setParticleLifetime(0.2, 0.5)

    entity.register(self, physics)
end

function playerBalloon:draw()
    entity.draw(self)

    local scale = 0.03

    love.graphics.setColor(unpack(self.color))
    love.graphics.draw(self.image, self.body:getX(), self.body:getY(), self.body:getAngle(), scale, scale, self.image:getWidth() * 0.5, self.image:getHeight() * 0.5)
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(self.emitter, self.body:getX(), self.body:getY(), self.body:getAngle())
end

function playerBalloon:update(dt)
    entity.update(self, dt)

    if self.emitting then
        self.emitter:emit(6)
    end

    self.emitter:update(dt)
end

return playerBalloon
