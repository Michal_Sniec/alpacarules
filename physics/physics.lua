local physics = class('physics')

function physics:initialize()
    self.world = love.physics.newWorld(0, 0)

    self.entities = {}
end

function physics:update(dt)
    self.world:update(dt)

    lume.each(self.entities, function(entity)
        if entity.update and not entity.fixture:isDestroyed() then entity:update(dt) end
    end)
end

function physics:draw()
    local players = lume.filter(self.entities, function(entity)
        return not entity.fixture:isDestroyed() and entity.fixture:getUserData().type == "player"
    end)
    lume.each(players, function(player)
        if player:isHoldingBalloon() then
            love.graphics.setColor(255, 255, 255)
            love.graphics.setLineWidth(2)
            love.graphics.line(player.body:getX(), player.body:getY(), player.balloon.body:getX(), player.balloon.body:getY() + player.balloon.radius)
            love.graphics.setLineWidth(1)
        end
    end)

    local sortedEntities = lume.sort(self.entities, function(entityA, entityB)
        return entityA.body:getY() < entityB.body:getY()
    end)
    lume.each(sortedEntities, function(entity)
        if entity.draw and not entity.fixture:isDestroyed() then entity:draw() end
    end)
end

return physics
