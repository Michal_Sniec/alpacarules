return{
    {
        y = 20,
        x = 20,
        scaleX = 1,
        scaleY = 1
    },
    {
        y = 20,
        x = love.graphics.getWidth() - 20,
        scaleX = -1,
        scaleY = 1
    },
    {
        y = love.graphics.getHeight() - 20,
        x = 20,
        scaleX = 1,
        scaleY = -1
    },
    {
        y = love.graphics.getHeight() - 20,
        x = love.graphics.getWidth() - 20,
        scaleX = -1,
        scaleY = -1
    }
}
