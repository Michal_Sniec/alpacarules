
local hitPoitsBar = class("hitPoitsBar")

local hitPoitsBarParams = require "physics.hitPointsBarParams"
function hitPoitsBar:initialize(player)
    self.basicWidth = love.graphics.getWidth() / 5
    self.height = 20
    self.maxHp = player.hitPoints
    self.color = lume.clone(player.color)
    table.insert( self.color,128 )
    self.index = player.index
    self.drawParams = hitPoitsBarParams[self.index]
end

function hitPoitsBar:draw(currentHp)
    local currentWidth = currentHp / self.maxHp
    -- love.graphics.setColor(128,128,128,128)
    -- love.graphics.rectangle("fill", self.drawParams.x + (self.drawParams.backWardsMultiplier * self.basicWidth), self.drawParams.y, self.basicWidth, self.height,20,9 )
    -- if(currentWidth>0) then
    --     love.graphics.setColor(unpack(self.color))
    --     love.graphics.rectangle("fill", self.drawParams.x + (self.drawParams.backWardsMultiplier * currentWidth), self.drawParams.y, currentWidth, self.height,20,9 )
    -- end

    local scale = 0.8

    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(assets.barBg, self.drawParams.x, self.drawParams.y, 0, self.drawParams.scaleX * scale, self.drawParams.scaleY * scale)
    love.graphics.setColor(unpack(self.color))
    love.graphics.draw(assets.barFg, self.drawParams.x + 13.5 * self.drawParams.scaleX * scale, self.drawParams.y, 0, self.drawParams.scaleX * scale * currentWidth, self.drawParams.scaleY * scale)
end
return hitPoitsBar
