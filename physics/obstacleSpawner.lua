
local obstacleSpawner = class("obstacleSpawner")

local obstacle = require "physics.obstacle"

function obstacleSpawner:initialize(physics)
    self.physics = physics
    local toRender = math.random(4, 6)
    for index = 1, toRender do
        obstacle:new(self.physics)
    end
    -- timer.every(8, function() self:spawn() end,100)

end


function obstacleSpawner:spawn()
    obstacle:new(self.physics)
end

return obstacleSpawner
