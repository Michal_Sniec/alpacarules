-- biblioteki
gamestate = require "lib.hump.gamestate"
timer = require "lib.hump.timer"
vector = require "lib.hump.vector"
signal = require "lib.hump.signal"

class = require "lib.middleclass"
lume = require "lib.lume"
tick = require "lib.tick"
baton = require "lib.baton"
anim8 = require "lib.anim8"

assets = require "lib.cargo" .init "assets"

-- stany
states = {}
states.menu = require "states.menu"
states.game = require "states.game"
states.playerSelect = require "states.playerSelect"
states.results = require "states.results"

playerTypes = require "physics.playerTypes"

utils = require "utils"

-- skróty bibliotek
C = lume.color
F = lume.lambda
