function love.conf(t)
    -- t.window.width = 1280
    -- t.window.height = 720
    t.window.fullscreen = true
    t.window.title = "Shockpaca"
    t.window.msaa = 16
end
