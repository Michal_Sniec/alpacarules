return {
    [1] = {
        controls = {
            left = {"key:left", "axis:leftx-"},
            right = {"key:right", "axis:leftx+"},
            up = {"key:up", "axis:lefty-"},
            down = {"key:down", "axis:lefty+"},
            action = {"key:rshift", "axis:triggerleft+"}
        },
        joystickIndex = 1
    },
    [2] = {
        controls = {
            left = {"key:a", "axis:rightx-"},
            right = {"key:d", "axis:rightx+"},
            up = {"key:w", "axis:righty-"},
            down = {"key:s", "axis:righty+"},
            action = {"key:q", "axis:triggerright+"}
        },
        joystickIndex = 1
    },
    [3] = {
        controls = {
            left = {"key:h", "axis:leftx-"},
            right = {"key:k", "axis:leftx+"},
            up = {"key:u", "axis:lefty-"},
            down = {"key:j", "axis:lefty+"},
            action = {"key:i", "axis:triggerleft+"}
        },
        joystickIndex = 2
    },
    [4] = {
        controls = {
            left = {"key:kp4", "axis:rightx-"},
            right = {"key:kp6", "axis:rightx+"},
            up = {"key:kp8", "axis:righty-"},
            down = {"key:kp5", "axis:righty+"},
            action = {"key:kp7", "axis:triggerright+"}
        },
        joystickIndex = 2
    }
}