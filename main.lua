love.graphics.setDefaultFilter("linear", "linear")

require "bootstrap"

function love.keypressed(k)
    if k == "escape" then
        love.event.quit()
    end
end

function love.load(args)
    tick.framerate = 60
    tick.rate = 1 / 60

    playerScores = {0,0,0,0}

    math.randomseed(os.time())
    font = love.graphics.newFont("assets/fonts/regular.ttf")
    love.graphics.setFont(font)
    fontBold = love.graphics.newFont("assets/fonts/bold.ttf")
    gamestate.registerEvents()

    gamestate.switch(states.menu)

    assets.quasiMotion:setLooping(true)
    assets.quasiMotion:setVolume(0.7)
    assets.quasiMotion:play()
end

function love.update(dt)
    timer.update(dt)
end
